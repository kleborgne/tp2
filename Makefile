/* copyright Les BG, Kilian, Guenael, Killian, Nicolas */
all: projet
projet: main.o
	gcc -o projet main.o

main.o: main.c
	gcc main.c -o main.o -Wall
